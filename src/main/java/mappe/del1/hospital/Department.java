package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * HospitalDepartmentClass
 *
 * @author Maiken Louise Brechan
 * @version March 2021
 */

public class Department {

    /**
     * class parameters
     * @param name of the department
     * @param list of employees
     * @param list of employees
     */
    private String departmentName;
    private ArrayList<Employee> employees = new ArrayList<>();
    private ArrayList<Patient> patients = new ArrayList<>();

    /**
     * Standarized get and set methods
     */

    public Department(String name){ this.departmentName=name; }

    public String getDepartmentName() { return departmentName; }

    public void setDepartmentName(String departmentName) { this.departmentName = departmentName; }

    public ArrayList<Employee> getEmployees(){ return employees; }

    public ArrayList<Patient> getPatients(){ return patients; }

    //For IDATT2001 teacher at NTNU -> thoughts on this method, see notes.txt

    /**
     * methods to add patient and employees
     * @param employee
     * @return boolean
     */
    public void addEmployee(Employee employee){
        if(!employees.contains(employee)){
            employees.add(employee);
        }
    }

    public void addPatient(Patient patient){
        if(!patients.contains(patient)){
            patients.add(patient);
        }
    }

    /**
     * standard equals and hashcode
     * the equals method checks if an object is the same as parametre
     * @return boolean
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName) &&
                Objects.equals(employees, that.employees) &&
                Objects.equals(patients, that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, employees, patients);
    }

    /**
     *Method to remove employee or patient from department's list
     * @param person
     * @throws RemoveException
     */

    public void remove(Person person) throws RemoveException {
            if (person instanceof Employee) {
                Employee employee = (Employee) person;
                if (!employees.contains(employee)) {
                    throw new RemoveException("Exception while removing employee " + employee.getFirstName());
                }
                employees.remove(employee);
                System.out.println(employee.getFirstName() + " removed");

            } else if (person instanceof Patient) {
                Patient patient = (Patient) person;
                if (!patients.contains(patient)) {
                    throw new RemoveException("Exception while removing patient " + patient.getFirstName());
                }
                patients.remove(patient);
                System.out.println(patient.getFirstName() + " removed");
            }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(departmentName + "\nPatients: ");
        for(Patient patient : patients){
            sb.append(patient.toString() + " ");
        }
        sb.append("\n Employees: ");
        for(Employee employee:employees){
            sb.append(employee.toString());
        }
        return sb.toString();
    }
}
