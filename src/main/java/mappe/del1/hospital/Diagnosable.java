package mappe.del1.hospital;

public interface Diagnosable {
    public void setDiagnosis(String diagnosis);
}