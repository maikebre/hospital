package mappe.del1.hospital;

import mappe.del1.hospital.healthpersonal.Nurse;
import mappe.del1.hospital.healthpersonal.doctor.Doctor;
import mappe.del1.hospital.healthpersonal.doctor.GeneralPractitioner;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;

/**
 * Employee class invoice from Person
 */
public class Employee extends Person{

    public Employee(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    @Override
    public String toString() {
        String a = "";
        if(this instanceof Doctor){
            a+= "Doctor ";
        }
        if(this instanceof Surgeon){
            a+= "Surgeon ";
        }
        if(this instanceof GeneralPractitioner){
            a+="General Practitioner ";
        }
        if(this instanceof Nurse){
            a+= "Nurse ";
        }
        return a + "\n"+super.toString();
    }
}
