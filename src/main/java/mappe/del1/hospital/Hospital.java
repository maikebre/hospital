package mappe.del1.hospital;
import java.util.ArrayList;

public class Hospital {
    /**
     * class parameters
     * @param name
     * @param list of departments
     */
    private final String hospitalName;
    private ArrayList<Department> departments = new ArrayList();

    public Hospital(String name) {
        this.hospitalName = name;
    }

    /**
     * Get and Set methods
     */
    public String getHospitalName() {
        return hospitalName;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     * Method adds department to Hospital
     * @param department
     * @return boolean
     */
    public boolean addDepartment(Department department) {
        for (Department dep : departments) {
            if (dep.getDepartmentName().equals(department.getDepartmentName())) {
                System.out.println("This department already exists.");
               return false;
            }
        }
        departments.add(department);
        return true;
    }

    /**
     * toString() prints out information about the hospital
     * @return toString
     */
    @Override
    public String toString() {
        String a="";
        for (Department department:departments ){
            a += department.toString() + "\n";
        }
        return "Hospital: " + hospitalName + '\n' +
                "departments: \n" + a;
    }
}
