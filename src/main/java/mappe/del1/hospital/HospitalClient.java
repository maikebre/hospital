package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import mappe.del1.hospital.healthpersonal.doctor.Doctor;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;

public class HospitalClient {
    public static void main(String[] args)throws RemoveException {

        /*
         * Filling hospital with test data
         */
        Hospital hospital = new Hospital("St.Olavs");
        HospitalTestData.fillRegisterWithTestData(hospital);

        /*
         * Removing employee
         */
        hospital.getDepartments().get(0).remove(hospital.getDepartments().get(0).getEmployees().get(0));

        /*
        Removing a Patient not added to the list
         */
        Patient patient=new Patient("Me", "Not You", "");
        try{
            hospital.getDepartments().get(0).remove(patient);
        }
        catch (RemoveException e){
            System.out.println(e.getMessage());
        }
    }
}
