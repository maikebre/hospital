package mappe.del1.hospital;

/**
 * Patients are diagnosable
 */

public class Patient extends Person implements Diagnosable{
    protected String diagnosis;

    public Patient(String firstName,String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Patient{" + super.toString() +
                "\n diagnosis:" + diagnosis;
    }

    /**
     * get and set methods
     * @return String diagnosis
     */
    protected String getDiagnosis(){
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }
}
