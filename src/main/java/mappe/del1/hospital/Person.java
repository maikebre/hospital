package mappe.del1.hospital;

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Constructor with exceptionhandling which in this case is commented out, due to the give test data.
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */

    public Person(String firstName, String lastName, String socialSecurityNumber){
        this.firstName=firstName;
        this.lastName=lastName;
        this.socialSecurityNumber=socialSecurityNumber;
        /*if(socialSecurityNumber.length()!=11){
            throw new IllegalArgumentException("Social security number cannot indicate birthdate after today");
        }
        else {
            this.socialSecurityNumber = socialSecurityNumber;
        }
         */
    }

    /**
     * Standard get and set methods for object attributes
     */

    public String getFirstName() { return firstName; }
    public String getLastName() { return lastName; }
    public String getSocialSecurityNumber() { return socialSecurityNumber; }
    public String getFullName(){ return firstName.concat(" "+lastName); }
    public void setFirstName(String newName){ this.firstName=newName; }
    public void setLastName(String newName){ this.lastName=newName; }

    /**
     * Social security number is not included in toString() to protect sensitive information.
     * @return
     */
    @Override
    public String toString() {
        return "Name: " + firstName + " " + lastName;
    }
}
