package mappe.del1.hospital.healthpersonal;

import mappe.del1.hospital.Employee;

//*A nurse cannot set Diagnosis

public class Nurse extends Employee {
    public Nurse(String firstName, String lastName, String socialSecurityNumber){
        super(firstName,lastName,socialSecurityNumber);
    }
    @Override
    public String toString(){
        return "Nurse: " + super.toString();
    }
}
