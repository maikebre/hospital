import mappe.del1.hospital.*;
import mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class RemoveExceptionTest {

    /*
    Declaring variables used throughout the tests.
     */
    Patient patient1;
    Employee employee1;
    Employee employee2;
    Department department1;

    /*
    Creating the test data
     */
    @BeforeEach
    void setup() {
       employee1 = new Employee("Jane", "Jansen", "123456");
       employee2 = new Employee("John", "Johnsen", "234567");
       patient1 = new Patient("John", "Doe", "345678");
       department1 = new Department("department1");

       department1.addEmployee(employee1);
       department1.addPatient(patient1);
    }

    /*
    Testing postive, not throwing an exception.
     */

    @Test
    void removePatientTest() throws RemoveException {
        assertDoesNotThrow(()-> department1.remove(patient1));
    }

    @Test
    void removeEmployeeTest() throws RemoveException{
        assertDoesNotThrow(()->department1.remove(employee1));
    }

    /*
    Checking that the exception is thrown.
     */

    @Test
    void removeNotAddedEmployee() throws RemoveException{
        Assertions.assertThrows(RemoveException.class, () -> {
            department1.remove(employee2);
        });
    }
}
